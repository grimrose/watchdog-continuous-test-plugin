package org.bitbucket.grimrose.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class WatchDogContinuousTestPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create 'watchdog', WatchDogContinuousTestPluginExtension
        project.task 'watching', type: WatchDogContinuousTestDefaultTask
    }

}


