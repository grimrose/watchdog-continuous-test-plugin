Watchdog Continuous Test Plugin
================================

[![Build Status](https://drone.io/bitbucket.org/grimrose/watchdog-continuous-test-plugin/status.png)](https://drone.io/bitbucket.org/grimrose/watchdog-continuous-test-plugin/latest)

This plugin support for the continuation of unit test.


## Getting Started

This plugin requires `Java 7`

## How to use

To use the plugin, add a dependency and apply it in your build.gradle:

```groovy
buildscript{
    repositories {
        maven {
            url "http://oss.sonatype.org/content/repositories/snapshots/"
        }
    }
    dependencies {
        classpath group: 'org.bitbucket.grimrose', name: 'watchdog-continuous-test-plugin', version: '1.0-SNAPSHOT'
    }
}

apply plugin: 'watchdog-continuous-test'

watchdog {
    tasks = ['test']
    dirs = sourceSets*.allSource*.srcDirs.flatten()
}
```

## Define tasks and directories

A watchdog instance has following properties:

 * `tasks` - tasks to be performed when changes and events occurs
 * `dirs` - directories to be monitored for changes and events.

## Use Task

In a command-line shell, call `watching` task:

```sh
$ gradle watching
```

## Contributions

Thanks for contributions. I welcome your issue reports or pull requests.

## Copyright and license

Copyright 2013 grimrose.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

  [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
