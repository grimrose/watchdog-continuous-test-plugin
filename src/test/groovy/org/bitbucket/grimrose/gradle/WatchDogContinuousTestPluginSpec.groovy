package org.bitbucket.grimrose.gradle

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class WatchDogContinuousTestPluginSpec extends Specification {

    def "should be instance of WatchDogContinuousTestDefaultTask"() {
        when:
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'watchdog-continuous-test'

        then:
        project.tasks.watching instanceof WatchDogContinuousTestDefaultTask
    }

    def "should be instance of WatchDogContinuousTestPluginExtension"() {
        when:
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'watchdog-continuous-test'

        then:
        project.watchdog instanceof WatchDogContinuousTestPluginExtension
    }

}
