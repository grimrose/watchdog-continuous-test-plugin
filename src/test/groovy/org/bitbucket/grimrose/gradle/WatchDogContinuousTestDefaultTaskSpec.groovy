package org.bitbucket.grimrose.gradle

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class WatchDogContinuousTestDefaultTaskSpec extends Specification {

    def "should be stop then tasks is empty"() {
        given:
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'watchdog-continuous-test'
        def extension = project.extensions.findByType(WatchDogContinuousTestPluginExtension)
        extension.dirs = []
        extension.tasks = []
        def sut = project.tasks.watching as WatchDogContinuousTestDefaultTask

        when:
        sut.taskExecutor.invoke()

        then:
        thrown AssertionError
    }

    def "should be stop then target task not found"() {
        given:
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'watchdog-continuous-test'
        def extension = project.extensions.findByType(WatchDogContinuousTestPluginExtension)
        extension.dirs = []
        extension.tasks = ['Hoge']
        def sut = project.tasks.watching as WatchDogContinuousTestDefaultTask

        when:
        sut.taskExecutor.invoke()

        then:
        thrown AssertionError
    }

}
