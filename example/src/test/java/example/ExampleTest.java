package example;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(WorkInProgress.class)
public class ExampleTest {

    @Test
    public void _instance_test() throws Exception {
        // Setup
        Example sut = new Example();
        // Verify
        assertThat(sut, instanceOf(Example.class));
    }

	@Test
    public void _should_be_true() throws Exception {
    	// Setup
        Example sut = new Example();
        // Verify
        assertThat(sut.doSomething(), is(true));
    }

}
