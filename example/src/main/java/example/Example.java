package example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Example {

    private static Logger logger = LoggerFactory.getLogger(Example.class);

    public boolean doSomething() {
        logger.debug("doSomething call.");
        return true;
    }

}
