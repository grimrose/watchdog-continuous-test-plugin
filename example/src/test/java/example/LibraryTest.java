package example;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class LibraryTest {

    @Test
    public void testSomeLibraryMethod() {
        Library classUnderTest = new Library();
        assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());
    }

    @Test
    public void _instance_test() throws Exception {
        // Setup
        Library sut = new Library();
        // Verify
        assertThat(sut, CoreMatchers.instanceOf(Library.class));
    }

}
