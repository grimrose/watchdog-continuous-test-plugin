package org.bitbucket.grimrose.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.tooling.GradleConnectionException
import org.gradle.tooling.GradleConnector
import org.gradle.tooling.ProjectConnection
import org.gradle.tooling.model.GradleProject
import org.gradle.tooling.model.GradleTask
import org.gradle.tooling.model.Task

import java.nio.file.FileSystems
import java.nio.file.Paths
import java.nio.file.WatchService

import static java.nio.file.StandardWatchEventKinds.*

class WatchDogContinuousTestDefaultTask extends DefaultTask {

    @TaskAction
    def watching() {
        watchService.polling()
    }

    RestrictedWatchService getWatchService() {
        def service = FileSystems.default.newWatchService()
        project.watchdog.dirs.each { File dir ->
            def path = dir.path
            project.logger.lifecycle "watching: ${path}"
            Paths.get(path).register(service, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_MODIFY)
        }
        new RestrictedWatchService(delegate: service, executor: taskExecutor)
    }

    TaskExecutor getTaskExecutor() {
        def targets = project.watchdog.tasks
        def rootDir = project.rootDir
        assert targets
        new TaskExecutor(rootDir: rootDir, targets: targets)
    }

    static class RestrictedWatchService {

        WatchService delegate

        TaskExecutor executor

        void polling() {
            while (true) {
                def key = delegate.take()
                key.pollEvents().each { event ->
                    if (event.kind() == OVERFLOW) return

                    invoke()
                }

                boolean valid = key.reset();
                if (!valid) break
            }
        }

        void invoke() {
            executor.invoke()
        }

    }

    static class TaskExecutor {

        File rootDir
        List<String> targets

        void invoke() {
            def connection = GradleConnector.newConnector().forProjectDirectory(rootDir).connect()
            try {
                def tasks = toTasks(connection)
                def out = new ByteArrayOutputStream()
                connection.newBuild()
                        .forTasks(tasks)
                        .setStandardOutput(out)
                        .setStandardError(out)
                        .run()
            } catch (GradleConnectionException e) {
                println e
            } finally {
                connection?.close()
            }
        }

        Collection<Task> toTasks(ProjectConnection connection) {
            Collection<Task> tasks = connection.getModel(GradleProject.class).tasks.findAll { GradleTask task ->
                targets.every { it == task.name }
            }
            assert tasks
            tasks
        }

    }

}
